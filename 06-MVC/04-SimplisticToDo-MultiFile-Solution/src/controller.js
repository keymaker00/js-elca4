(function (global) {
    'use strict';

    var controller = {

        init: function () {

            var model = global.todo.createModel();
            var summaryView = new global.todo.SummaryView();
            var todoView = new global.todo.TodoView();
            var todoListView = new global.todo.TodoListView();

            model.bind('changed', updateUi);

            todoView.bind('itemChanged', updateTodo);
            todoView.bind('itemAdded', addItem);
            todoListView.bind('itemRemoved', removeItemAtIndex);

            summaryView.render(model.todos);
            todoView.render(model.newTodo);
            todoListView.render(model.todos);

            function updateTodo(todoText) {
                model.updateNewTodo(todoText);
            }
            function addItem() {
                model.addNewTodo();
            }
            function removeItemAtIndex(index) {
                model.removeTodoAtIndex(index);
            }

            function updateUi() {
                summaryView.render(model.getTodoList());
                todoView.render(model.getNewTodo());
                todoListView.render(model.getTodoList());
            }

        }
    };

    global.todo = global.todo || {};
    global.todo.controller = controller;

})(window);
