(function(global) {
    'use strict';

    MicroEvent.mixin(TodoListView);
    function TodoListView() {

        var self = this;

        var todoListElem = document.getElementById('do');

        self.render = function(todos){
            var todo;

            todoListElem.innerHTML = '';
            for(var i = 0, len = todos.length; i < len  ; i++ ){
                todo = todos[i];

                var li = document.createElement('li');
                li.setAttribute('data-index', i);
                li.classList.add('clearfix');
                li.textContent = todo.text;

                var span = document.createElement('span');
                span.classList.add('pull-right');

                var button = document.createElement('button');
                button.setAttribute('class', "btn btn-xs btn-danger remove glyphicon glyphicon-trash");
                button.addEventListener('click', (function(index){
                    return function(){self.trigger('itemRemoved', index)};
                })(i));

                span.appendChild(button);
                li.appendChild(span);
                todoListElem.appendChild(li);
            }
        }
    }

    global.todo = global.todo || {};
    global.todo.TodoListView = TodoListView;

})(window);
