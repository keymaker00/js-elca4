(function(global) {
    'use strict';

    function SummaryView(){

        var self = this;

        var summaryElem = document.getElementById('count');

        self.render = function (todos) {
            summaryElem.textContent = todos.length + ' items'
        }
    }


    global.todo = global.todo || {};
    global.todo.SummaryView = SummaryView;

})(window);
