(function(global) {
    'use strict';

    // Getting a reference to a controller like this does not work, since the corresponding file only gets loadet later:
    //var controller = global.todo.controller;

    MicroEvent.mixin(TodoView);
    function TodoView() {

        var self = this;

        var todoInputElem =  document.getElementById('input');
        var addBtn = document.getElementById('addBtn');

        todoInputElem.addEventListener('blur', function(){
            self.trigger('itemChanged', todoInputElem.value);
        });

        addBtn.addEventListener('click', function(){
            self.trigger('itemAdded');
        });

        self.render = function(todo){
            todoInputElem.value = todo.text;
        }
    }


    global.todo = global.todo || {};
    global.todo.TodoView = TodoView;

})(window);
