todo.controller.init();

// Layout Logic
enableTab(location.hash);

function enableTab(hash) {
    if (hash === '') hash = '#main';
    document.querySelectorAll('.container .row').forEach(function (e) {
        e.style.display = 'none';
    });
    document.getElementById(hash.substring(1)).style.display = 'block';
}

window.addEventListener('hashchange', function(){
    enableTab(location.hash);
});
