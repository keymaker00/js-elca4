### Automated Tests

To run tests in the browser just open `test/browser.index.html` in the browser.

To get live-refresh in the default browser run:

    npm install #just once
    npm test
    
To run test in node without the browser:

    npm install #just once
    npm test-node
