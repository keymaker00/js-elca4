(function () {
    'use strict';

    function createModel() {

        var newTodo = { text: '' };
        var todoList = [];

        var model = {
            getNewTodo: function(){return newTodo;},
            getTodoList: function(){return todoList},
            updateNewTodo: updateNewTodo,
            addNewTodo: addNewTodo,
            removeTodoAtIndex: removeTodoAtIndex,
            todos: [],
            newTodo: { text: '' }
        };
        MicroEvent.mixin(model);
        return model;

        function updateNewTodo(text){
            newTodo.text = text;
            model.trigger('changed');
        }

        function addNewTodo(){
            todoList.push(newTodo);
            newTodo = { text: '' };
            model.trigger('changed');
        }

        function removeTodoAtIndex(index){
            todoList.splice(index, 1);
            model.trigger('changed');
        }
    }

    MicroEvent.mixin(TodoView);
    function TodoView() {

        var self = this;

        var todoInputElem = document.getElementById('input');
        var addBtn = document.getElementById('addBtn');

        todoInputElem.addEventListener('blur', function () {
            self.trigger('itemChanged', todoInputElem.value);
        });

        addBtn.addEventListener('click', function () {
            self.trigger('itemAdded');
        });

        self.render = function (todo) {
            todoInputElem.value = todo.text;
        }
    }

    MicroEvent.mixin(TodoListView);
    function TodoListView() {

        var self = this;

        var todoListElem = document.getElementById('do');

        self.render = function (todos) {
            var todo;

            todoListElem.innerHTML = '';
            for (var i = 0, len = todos.length; i < len; i++) {
                todo = todos[i];

                var li = document.createElement('li');
                li.setAttribute('data-index', i);
                li.classList.add('clearfix');
                li.textContent = todo.text;

                var span = document.createElement('span');
                span.classList.add('pull-right');

                var button = document.createElement('button');
                button.setAttribute('class', "btn btn-xs btn-danger remove glyphicon glyphicon-trash");
                button.addEventListener('click', (function (index) {
                    return function () { self.trigger('itemRemoved', index) };
                })(i));

                span.appendChild(button);
                li.appendChild(span);
                todoListElem.appendChild(li);
            }
        }
    }

    function SummaryView() {

        var self = this;

        var summaryElem = document.getElementById('summary');

        self.render = function (todos) {
            summaryElem.textContent = todos.length + ' items'
        }
    }


    var controller = {

        init: function () {

            var model = createModel();

            model.bind('changed', updateUi);

            var summaryView = new SummaryView();
            var todoView = new TodoView();
            var todoListView = new TodoListView();

            todoView.bind('itemChanged', updateTodo);
            todoView.bind('itemAdded', addItem);
            todoListView.bind('itemRemoved', removeItemAtIndex);

            summaryView.render(model.todos);
            todoView.render(model.newTodo);
            todoListView.render(model.todos);

            function updateTodo(todoText) {
                model.updateNewTodo(todoText);
            }
            function addItem() {
                model.addNewTodo();
            }
            function removeItemAtIndex(index) {
                model.removeTodoAtIndex(index);
            }

            function updateUi(){
                summaryView.render(model.getTodoList());
                todoView.render(model.getNewTodo());
                todoListView.render(model.getTodoList());
            }

        }
    };
    controller.init();
})();
