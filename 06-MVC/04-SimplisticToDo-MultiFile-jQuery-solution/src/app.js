todo.controller.init();

// Layout Logic
enableTab(location.hash);

function enableTab(hash) {
    if (hash === '') hash = '#main';

    $('.container .row').hide();
    $(hash).show();
}

window.addEventListener('hashchange', function(){
    enableTab(location.hash);
});
