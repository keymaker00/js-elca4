(function(global) {
    'use strict';

    function SummaryView(){

        var self = this;

        var summaryElem = $('#count');

        self.render = function (todos) {
            summaryElem.text(todos.length + ' items');
        }
    }


    global.todo = global.todo || {};
    global.todo.SummaryView = SummaryView;

})(window);
