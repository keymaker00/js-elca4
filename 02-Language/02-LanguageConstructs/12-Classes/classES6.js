class Person {
    constructor(first, last) {
        this.first = first;
        this.last = last;
    }

    fullName() {
        return this.first + ' ' + this.last;
    }

    fullNameReversed(){
        return this.last + ', ' + this.first;
    }
}

const pers = new Person('John', 'Doe');
console.log(pers.fullName());
console.log(pers.fullNameReversed());

console.log(typeof pers);
console.log(typeof Person);
console.log(pers.constructor);
console.log(pers.__proto__ === Person.prototype);
