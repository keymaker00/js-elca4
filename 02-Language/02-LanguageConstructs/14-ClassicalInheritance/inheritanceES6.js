class Person {
    constructor(firstName, lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    fullName() { return this.firstName + ' ' + this.lastName};
}

const pers = new Person('John', 'Doe');
console.log(pers.fullName());
console.log(typeof Person);

class Employee extends Person{
    constructor(employer, firstName, lastName){
        super(firstName, lastName);
        this._employer = employer;
    }

    get employer() {
        return this._employer;
    }

    set employer(val) {
        this._employer = val.toUpperCase();
    }
}
//
const empl = new Employee('Fight Club', 'Tyler', 'Durden');
// console.log(empl.fullName());
empl.employer = 'Gugus';
console.log(empl.employer);

console.log(typeof empl);
console.log(typeof Employee);
console.log(empl.constructor);
console.log(empl.__proto__ === Employee.prototype);
console.log(empl.__proto__.__proto__ === Person.prototype) ;
