var assert = require('assert');

function Person(name) {
    console.log('Person constructor called...');
    this.name = name;
};
Person.prototype.getName = function() {
    return this.name;
};

function Employee(name, employer) {
    // rent-a-constructor
    Person.call(this, name);
    // extend with more properties
    this.employer = employer;
};
Employee.prototype = new Person();

// Alternative:
//Employee.prototype = Object.create(Person.prototype);


var empl = new Employee("Tyler Durden", "TheClub");

// Check properties
assert(empl.hasOwnProperty('name'));
assert(empl.hasOwnProperty('employer'));

// Follow the prototype chain
assert(!empl.hasOwnProperty('getName'));
assert(!empl.__proto__.hasOwnProperty('getName'));
assert(empl.__proto__.__proto__.hasOwnProperty('getName'));

console.log(empl.getName());


// console.log('Check type: ' + (empl instanceof Person));
