// Get argument names /////////////////////
// For a better implementation see Angular 1.3.14 source line 3442
var ARGUMENT_NAMES = /([^\s,]+)/g;

function getParamNames(func) {
    var fnStr = func.toString();
    var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
    if(result === null)
        result = [];
    return result;
}

console.log(getParamNames(getParamNames));
console.log(getParamNames(function (a,b,c,d){}));


///////////////////////////////////////
function logCallee(){
    console.log(logCallee.caller); // not allowed in strict mode!
}

function test(){
    logCallee();
}

console.log(test());

//////////////////////////////////////
function printStack(){
    var e = new Error("stacktrace");
    var stack = e.stack;

    console.log(stack);
}
printStack();
