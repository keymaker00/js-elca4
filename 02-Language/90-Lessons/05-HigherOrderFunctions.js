/*
 This lesson uses the lodash library: https://lodash.com/
 Lodashes utility functions exposed by the _ variable
 */
lesson("About Higher Order Functions", function () {

  learn("how to use filter to return array items that meet a criteria", function () {
    var numbers = [1,2,3];
    var odd = _.filter(numbers, function (x) { return x % 2 !== 0 });

    expect(odd).toEqual(FILL_ME_IN);
    expect(odd.length).toBe(FILL_ME_IN);
    expect(numbers.length).toBe(FILL_ME_IN);
  });

  learn("how to use 'map' to transform each element", function () {
    var numbers = [1, 2, 3];
    var numbersPlus1 = _.map(numbers, function(x) { return x + 1 });

    expect(numbersPlus1).toEqual(FILL_ME_IN);
    expect(numbers).toEqual(FILL_ME_IN);
  });

  learn("how to use 'reduce' to update the same result on each iteration", function () {
    var numbers = [1, 2, 3];
    var reduction = _.reduce(numbers,
            function(/* result from last call */ memo, /* current */ x) { return memo + x }, /* initial */ 0);

    expect(reduction).toBe(FILL_ME_IN);
    expect(numbers).toEqual(FILL_ME_IN);
  });

  learn("how to use 'forEach' for simple iteration", function () {
    var numbers = [1,2,3];
    var msg = "";
    var isEven = function (item) {
      msg += (item % 2) === 0;
    };

    _.forEach(numbers, isEven);

    expect(msg).toEqual(FILL_ME_IN);
    expect(numbers).toEqual(FILL_ME_IN);
  });

  learn("about using 'every' to test whether all items pass condition", function () {
    var onlyEven = [2,4,6];
    var mixedBag = [2,4,5,6];

    var isEven = function(x) { return x % 2 === 0 };

    expect(_.every(onlyEven, isEven)).toBe(FILL_ME_IN);
    expect(_.every(mixedBag, isEven)).toBe(FILL_ME_IN);
  });

  learn("about using 'some' to test if any items passes condition" , function () {
    var onlyEven = [2,4,6];
    var mixedBag = [2,4,5,6];

    var isEven = function(x) { return x % 2 === 0 };

    expect(_.some(onlyEven, isEven)).toBe(FILL_ME_IN);
    expect(_.some(onlyEven, isEven)).toBe(FILL_ME_IN);
  });

  learn("about using range to generate an array", function() {
      expect(_.range(3)).toEqual(FILL_ME_IN);
      expect(_.range(1, 4)).toEqual(FILL_ME_IN);
      expect(_.range(0, -4, -1)).toEqual(FILL_ME_IN);
  });

  learn("about using flatten to make nested arrays easy to work with", function() {
      expect(_([ [1, 2], [3, 4] ]).flatten().value()).toEqual(FILL_ME_IN);
  });

  learn("should use chain() ... .value() to use multiple higher order functions", function() {
      var result = _.chain([ [0, 1], 2 ])
                       .flatten()
                       .map(function(x) { return x+1 } )
                       .reduce(function (sum, x) { return sum + x })
                       .value();

      expect(result).toEqual(FILL_ME_IN);
  });

});

