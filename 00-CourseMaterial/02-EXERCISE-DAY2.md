## Exercise 1: AJAX with Callbacks
Inspect the example `05-Async/20-Promises/90-Exercise-jQuery-callbacks`.

Start the Node server on the commandline:

	cd _server
	npm install
	npm start

This server provides a HTTP-based API from which you can get 9 words that make up a complete sentence. You can get the first word from the following URL:

	http://localhost:3456/word/0
	
The last parameter can be varied between 0 and 8.

Inspect the web-page `app/index.html`. The logic in the script loads the first word from the server and renders it on the page.

Extend the example, so that all the words of the sentence are loaded from the server, and the complete sentence is rendered on the page.

Try two approaches for your implementation:

- All words are immediately queried from the server. The complete sentence should be rendered on the page once all the words have been loaded.

- Query one word after the other from the server. The next word should only be queried when the previous word has been received (observe the waterfall pattern in the network inspector of the developer tools). Each word should be rendered on the page as soon as it has been received from the server.

Use jQuery with callbacks to implement the logic.



## Exercise 2: AJAX with Promises
Inspect the example `05-Async/20-Promises/91-Exercise-promises`.

Important: Do not use Internet Explorer to run this example since it does not implement native promises!

Start the Node server on the commandline:

	cd _server
	npm install
	npm start

This server provides a HTTP-based API from which you can get 9 words that make up a complete sentence. You can get the first word from the following URL:

	http://localhost:3456/word/0
	
The last parameter can be varied between 0 and 8.

Inspect the web-page `app/index.html`. The logic in the script loads the first word from the server and renders it on the page.

Extend the example, so that all the words of the sentence are loaded from the server, and the complete sentence is rendered on the page.

Try two approaches for your implementation:

- All words are immediately queried from the server. The complete sentence should be rendered on the page once all the words have been loaded.

- Query one word after the other from the server. The next word should only be queried when the previous word has been received (observe the waterfall pattern in the network inspector of the developer tools). Each word should be rendered on the page as soon as it has been received from the server.

Use jQuery and Promises to implement the logic.



## Exercise 3: Plain MVC

Change the ToDo List application from example `05-MVC/02-SimplisticToDo-NoMVC` to use the MVC-pattern. The goal is to extract the state of the application from the DOM into a JavaScript object model.




## Exercise 4: Modularisation - Plain JavaScript

Use the solution for the TODO-List from exercise 3.

Split the implementation `src/main.js` into multiple files. Typically each controller, model and view should be put into its own file. 

Use the namespace pattern and IIFEs for isolation. 



## Exercise 5: Modularisation - WebPack

Inspect the example `06-MVC/05-SimplisticToDo-ES5-import-Webpack`.  

Start the example like this:

	npm install # just once
	npm start
	
Then navigate to:

	http://localhost:8080/webpack-dev-server/

This example uses ECMAScript 2015 modules (`import` and `export`) and WebPack for bundling. Inspect the sources and inspect what the browser loads at runtime.



## Exercise 6: CourseRater with WebPack, TypeScript and ES2015 Modules

Convert the application in `03-TypeScript/91-CourseRaterExercise/courserater-es5` to a WebPack-project that uses TypeScript and ES2015 Modules.

Option 1: Start "from Scratch".

Option 2: Start with `03-TypeScript/91-CourseRaterExercise/courserater-webpack-skeleton-1`. In this skeleton project WebPack is already configured. You can start writing TypeScript code that implements the functionality. Read the instructions in  `README` to get started.

Option 3: Start with `03-TypeScript/91-CourseRaterExercise/courserater-webpack-skeleton-2`. In this project WebPack is already configured and most of the functionality is already implemented.  Implement the "statistic feature" which shows the count and average of all ratings. Read the instructions in `README` to get started.





