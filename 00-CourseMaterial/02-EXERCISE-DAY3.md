# Exercises Angular

## Exercise 1: Creating an Angular App-Skeleton with Angular CLI

Install Angular CLI as a global npm package:

	npm install -g angular-cli
	
Create a new Angular application:

	ng new my-first-app
	
Inspect the project that has been created. Try to understand the setup (where is the actual source code, where are the artifacts for deployment ...)

Serving the app:

	cd my-first-app
	ng serve
	
Navigate a browser to `http://localhost:4200/`. Inspect the app. Open the browser developer tools and inspect the resources the browser actually loads over the network.

Serve a production build:

	ng serve -prod
	
Open the browser developer tools and inspect once more the resources the browser actually loads over the network. What has changed? Is this an optimal situation?

Add another component:

	ng generate component hello
	
Modify the component to display "Hello World" in the browser. Extend the app so that this new "hello"-component is used.

Optional: Extend the test in `app.component.spec.ts` so that it checks that the new component is rendered as a child component.



## Exercise 2: Angular 2 UI

Begin with the example `50-Angular2/90-CourseRater-ui-skeleton`. Get the example running, see the README.

The example implements all the components of the UI of the Course-Rater app as skeletons without functionality. Your task is to implement the following functionality:

- A "live-preview" should be rendered on the left below the input controls. It should show the current content of the grade and name inputs.

- The "live-preview" should only be shown, when the form is valid. The "add" button should only be enabled when the form is valid.

- A rating entry should be added to the list when the "Add"-button is clicked.

- The "entered" date in the list should be formatted in a pretty way.

- Each entry in the rating list should have a "Remove" button. If this button is clicked, the entry should be removed from the list.

- If there are no entries in the list, the whole table should not be rendered.

- Below the table a counter should show the current count of rating entries in the list. 

Optional: Extract a new component `NewRatingComponent` containing the form on the left for adding a new rating.


## Exercise 3: Angular 2 Backend Access

The project `50-Angular2/90-CourseRater-ui-skeleton` contains a simple API-Server implementing basic CRUD functionality for our Course-Rating application.
Start the server with the following commands:

	cd _server
	npm install #just once
	npm start
	
You should now get an array with one rating at the url: `http://localhost:3456/courserater/rest/ratings`.

Your task is no to access this backend API from the Course-Rater application:
- When the application is loaded, all the ratings should be loaded from the server
- When a rating is added, it should be saved to the server
- When a rating is deleted, it should be deleted from the server.

Introduce a data service that abstracts the server interactions.  
Inject the service into the component(s) that need to communicate with the server.


The API implemented by the REST-Endpoint is described in the table below:


HTTP-Method   | URL (example) 										| Request-Body
------------- | ------------- 										|-------------
GET			  | http://localhost:3456/courserater/rest/ratings    	|
GET			  | http://localhost:3456/courserater/rest/ratings/1    |
POST		  | http://localhost:3456/courserater/rest/ratings		| { "name": "John", "grade": "4", "entered":"2014-04-15"}
PUT		 	  | http://localhost:3456/courserater/rest/ratings/1	| { "id":1, "name": "John","grade": "2", "entered":"2014-04-15"}
DELETE		  | http://localhost:3456/courserater/rest/ratings/1	| 

Note that all responses are wrapped in a response object with a `data` property.
This is a typical security measure of JSON endpoints. See: http://stackoverflow.com/questions/3503102/what-are-top-level-json-arrays-and-why-are-they-a-security-risk

