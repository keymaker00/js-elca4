## Exercise 1: Language Constructs

In this exercise you get to know certain JavaScript language constructs by completing "automated lessons":

- Change into the directory `02-Language/90-Lessons`
- Start a web server in the directory and open `index.html` in the browser. Either use `lite-server` (installed globally via npm) on the commandline or open the file from WebStorm by choosing 'Open in Browser'.
- You will see some failing tests.
- Open the corresponding JavaScript file in an editor. i.e: `01-Basics.js`. Read the code of corresponding lesson. 
- Edit the code, so that the first Tests should be passing.
- Refresh the browser. The test should turn green.
- Advance to the next test.



## Exercise 2: Capturing A Context with a Closure

Inspect the code in the example `03-Patterns/05-CapturingScope/`. Click on the numbers and the Button: Did you expect the given behaviour?  

Change the code, so that:

- when you click on a number then that number is displayed in the alert dialog 
- clicking the button shows an increasing number in the alert

Use closures to achieve that effect.



## Exercise 3: From Spaghetti to Modules

Inspect the example `03-Patterns/03-Spaghetti`: Include the new functionality on the page by un-commenting the commented code in `index.html`.  
Why is there a side-effect form the newly included functionality on the existing functionality?

Change the example, so that there are no side-effects between the existing and the newly included functionality.

Use IIFEs/closures to wrap each functionality in a module that is completely isolated.



## Exercise 4: ES6 Language Constructs

In this exercise you get to know new ES2015 language constructs by completing "automated lessons":

- Change into the directory `60-ES2015/90-Lessons`
- Start a web server in the directory and open `index.html` in the browser. Either use `http-server` (installed via npm) on the commandline or open the file from WebStorm by choosing 'Open in Browser'.
- You will see some failing tests.
- Open the corresponding JavaScript file in an editor. i.e: `01-Variables.js`. Read the code of corresponding lesson. 
- Edit the code, so that the first Tests should be passing.
- Refresh the browser. The test should turn green.
- Advance to the next test.



## Exercise 5: Capturing a Context in ES2015

Go back to the example `02-Language/92-CapturingScope/`.

Try to solve the problem with ES2015 constructs (`let` and `for-of`-loop). The solution should be much more intuitive than with ES5.



## Exercise 6: From Spaghetti to ES6 Modules

Inspect the example `10-ES2015/92-Spaghetti`. It is a variation of the "spaghetti exercise" we solved before.

Try to solve the problem with ES2015 modules. The solution should be much more intuitive than with ES5.

To see how ES2015 module loading is bootstrapped with `SystemJS`, see the example `10-ES2015/01-LanguageFeatures/09-Modules-Web`.



