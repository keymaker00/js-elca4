## JavaScript Workshop

### Important:

**For questions or problems concerning the instructions below, please contact:   jonas.bandi@ivorycode.com.**

Every participant should bring his own laptop prepared with the following instructions.

Make sure that you have unrestricted access to the internet! In the steps below you will have to access GitHub and npmjs.org. Some corporate proxies block these sites or block access over https/git!



## Software Installation
For the workshop the following software should be installed.  

**The version numbers don't have to match exactly, but should be pretty close!**

### Git
A recent git installation is needed to download the sample code and exercises.  
Git can be downloaded from here: <https://git-scm.com/download/>

Check:  

	> git --version                                                             
	git version 2.6.4



### Node.js & NPM 
Node and NPM are the fundament for the JavaScript toolchain.  
The Node installer can be downloaded here: <http://nodejs.org/download/>

Please install a version of Node > 6.

**Advanced instructions for OSX/Linux:** If you don't want to install global packages with `sudo` in the steps below, you can either install [Node Version Manager](https://github.com/creationix/nvm) (recommended) or you can perform the instructions here: <https://docs.npmjs.com/getting-started/fixing-npm-permissions>

Check:

	> node --version
	v6.9.0
	> npm --version
	3.10.8
	

### Global NPM Packages

We want to install some JavaScript development tools globally, so that they can be used from the commandline.

**OSX/Linux:**

	sudo npm install -g lite-server http-server angular-cli



**Windows:** run the following commands in a Administrator command prompt:

	npm install -g lite-server http-server angular-cli


Ignore some scary warnings during the installation. Everything is ok, as long as the final output of the installation command is not an `npm ERR ...`
	
Background info: As default (if you did not execute the advanced instructions above to avoid `sudo`) the installation of those tools is placed in `/usr/local/lib/` on OSX/Linux or in `C:\Program Files\nodejs` on Windows. To uninstall the packages, you can always delete the directory `node_modules` there.




### Browser
A recent Chrome and/or Firefox Browser must be available.  



### WebStorm
The WebStorm IDE from JetBrains should be installed.  
There is a free 30 day trial version of WebStorm available here: <http://www.jetbrains.com/webstorm/>.

WebStorm is not a requirement for the workshop. However the examples and demos will be shown with WebStorm. It is up to the attendees to use any other editor of their preference.  
Note: IntelliJ IDEA Ultimate supports the same features as WebStorm.



## Preparation Course Material

All the course material will be provided in the following repository:

	https://bitbucket.org/jonasbandi/js-elca4/
	
Please clone the repo like this:

	git clone https://bitbucket.org/jonasbandi/js-elca4.git
	

To update the repo later:

	cd js-elca4
	git pull
	


Please execute the following instructions to verify, that your setup is ok.

From within the cloned repository execute the following commands:

	cd 01-Intro/05-SimplisticToDo-ES6
	npm install

	
The above step should download all the dependencies to build and run the demo application. 

The build should now work by running the following command:

	npm start


A browser should open with a simple ToDo-Application.


The last step is to create a Angular 2 project skeleton, to save time during the course.  
From within the cloned repository execute the following commands:

	ng new first-app
	cd first-app
	ng serve
	
The above steps can take several minutes to complete. Once the steps finished you should open a browser at `http://localhost:4200` and see a simple page that displays `app works`.



