module.exports = {
    entry: './src/app',
    output: {
        filename: 'build/bundle.js'
    },
    devtool: 'source-map',
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        loaders: [
            {test: /\.ts$/, loader: 'ts', exclude: /node_modules/}
        ]
    }
};
