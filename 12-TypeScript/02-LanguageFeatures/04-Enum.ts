enum Direction {
    Up = 1,
    Down,
    Left,
    Right
}

console.log(Direction.Down); // Enums have a numeric type
console.log(Direction[Direction.Down]); // You can get the name of a enum value

function evalDirection(direction: Direction): string {
    if (direction === Direction.Up) {
        return ':-)';
    }
    else {
        return ':-(';
    }
}

const result: string = evalDirection(Direction.Down);
console.log(result);
