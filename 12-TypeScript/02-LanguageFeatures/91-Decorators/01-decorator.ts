// Transpile the script with `tsc` (`tsconfig.json`) has to be present
// Run the script with `node build/01-decorator.js`

// simple decorator annotation
function superhero(target){
  target.isSuperhero = true;
  target.prototype.isSupercat = true;
}

@superhero
class Cat {
  constructor(name: string){}
  meow(){ return 'MEOOOW!'}
}

console.log('Is superhero? ' + Cat['isSuperhero']);

let garfield = new Cat('garfield');
console.log('Supercat:' + garfield['isSupercat']);
console.log(garfield.meow());
