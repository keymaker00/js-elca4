namespace MyApp {

    export class MyThing {
        doSomething(){
            console.log('something');
        }
    }


}

const myThing = new MyApp.MyThing();
myThing.doSomething();


// Note: namespaces and ES2015 modules are typically not used together ...
