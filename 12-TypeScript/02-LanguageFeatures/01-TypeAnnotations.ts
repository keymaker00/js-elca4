function getMessage(name: string): number {
    const greeting: string = 'Hello';
    return 42;
}
const message:number = getMessage('Tyler');
console.log(message);


// Any type
function sayHello2(name) {
    console.log('Hello ' + name);
}
sayHello2(true);


// Union types
function sayHello3(name: string | number) {
    console.log('Hello ' + name);
}
sayHello3(42);


// Type Alias
type HelloArg = string | number;
function sayHello4(name: HelloArg) {
    console.log('Hello ' + name);
}
sayHello4('Tyler');


// Generics
type Container<T> = { value: T };

function getContainer(name: string) : Container<string> {
    return {value: name}
}
const container = getContainer('Hello');
console.log(container.value);
