interface IPerson {
    name: string,
    age: number
}

interface ITest {
    test: string
}

var o = {
    name: 'Tyler',
    age: 42
};


// Structural typing
const p1: IPerson = o;

function test(pers : IPerson){

}

test(o)

// TODO:
// - Optional types
//

// Classes can implement interfaces
class Child implements IPerson, ITest {
    name: string;
    age: number;
    test: string = 'asdf'
}
const p2 = new Person();


// Interfaces are types
const greeter = (p: IPerson):void => console.log(p.name);
const producer = ():IPerson => (new Child());

greeter(p1);
const p3 = producer();


// an interface can describe a function
interface Creator {
    (name:string): IPerson
}

const factory: Creator = function (name) {
    return {
        name: name,
        age: 43
    }
};

const p4: IPerson = factory('Durden');
