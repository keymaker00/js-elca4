Install project dependencies:

    npm install

Run development build:

    npm start
    
-> Then go to `http://localhost:5678`

Build a production build:

    npm run build
    
    
