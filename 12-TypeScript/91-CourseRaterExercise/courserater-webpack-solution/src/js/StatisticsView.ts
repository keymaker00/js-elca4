import {IRating} from "./Model";

export class StatisticsView {
    private ratingsCount: JQuery;
    private ratingsAverage: JQuery;

    constructor() {
        this.ratingsCount = $('#ratingsCount');
        this.ratingsAverage = $('#ratingsAverage');
    }

    public render(ratings: Array<IRating>): void {
        var statistics = calculateStatistics(ratings);

        this.ratingsCount.text(statistics.count);
        this.ratingsAverage.text(statistics.average);
    }
}


function calculateStatistics(ratings: Array<IRating>) {

    var ratingsSum = ratings.reduce( (sum, rating) => sum + rating.grade, 0);

    return {
        count: ratings.length,
        average: (ratingsSum / ratings.length) || 0
    }

}
