import {Model} from "./Model";
import {StatisticsView} from "./StatisticsView";

export class StatisticsController {

    constructor(private model: Model, private statisticsView: StatisticsView) {

        $(model).on('change', () => this.renderViews());

        this.renderViews();
    }

    renderViews() : void{
        this.statisticsView.render(this.model.ratings);
    }
}
