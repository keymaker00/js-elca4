(function(global) {
    'use strict';

    function StatisticsView() {

        var self = this;

        var ratingsCount = $('#ratingsCount');
        var ratingsAverage = $('#ratingsAverage');

        self.render = function(ratings){

            var statistics = calculateStatistics(ratings);

            ratingsCount.text(statistics.count);
            ratingsAverage.text(statistics.average);
        };

        function calculateStatistics(ratings) {

            var ratingsSum = ratings.reduce(function(sum, rating){ return sum += rating.grade}, 0 );

            return {
                count: ratings.length,
                average: (ratingsSum / ratings.length) || 0
            }

        }
    }

    global.courserater = global.courserater || {};
    global.courserater.StatisticsView = StatisticsView;

})(window);
