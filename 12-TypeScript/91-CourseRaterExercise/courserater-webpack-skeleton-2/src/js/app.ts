import 'jquery';

import {Model} from "./Model";
import {NewRatingView} from "./NewRatingView";
import {RatingController} from "./RatingController";
import {RatingListView} from "./RatingListView";

// Wire the app
const model = new Model();
const newRatingView = new NewRatingView();
const ratingListView = new RatingListView();
new RatingController(model, newRatingView, ratingListView);


// Layout Logic
enableTab(location.hash);

function enableTab(hash: string) {
    if (hash === '') hash = '#main';
    $('.container section').hide();
    $(hash).show();
}

window.addEventListener('hashchange', e => {
    enableTab(location.hash)
});
