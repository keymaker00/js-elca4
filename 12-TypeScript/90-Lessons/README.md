Install dependencies:

    npm install

Start the lessons:

    npm start
    
A browser should open automatically at `localhost:7777`

To complete the lessons edit the corresponding typescript files i.e. `00-Basics.ts`.
