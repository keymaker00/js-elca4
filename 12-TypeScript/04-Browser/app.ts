// The code below has many errors, can you spot them?

var movie = { title: "Memento", year: 2000, imdb: 8.5};
var rating = movie.imdb;



function Point(x, y) {
  this.x = x;
  this.y = y;
}
Point.prototype.distance = function() {
  return Math.sqrt(this.x * this.x + this.y * this.y);
};



function isPast(date) {
  var now = new Date().getTime();
  return date.getTime() < now;
}



function ask(question) {
  var answer = prompt(question);
  var answerView = document.getElementById('answer');
  answerView.innerHTML = answer;
}



function handleLoad() { return () => console.log('loaded'); }
document.onload = handleLoad();



function newCoinToss() {
  return Math.random() > 0.5 ? 'HEADS' : 'TAILS';
}
var tosses = [1,2,3].map(newCoinToss);

var allHeads = tosses.every(function(toss) {
  return toss = 'HEADS';
});
if (allHeads) console.log(allHeads.length, 'heads in a row!');



document.addEventListener('keydown', function(event) {
  console.log(event.clientX, event.clientY);
});
