// The code below has a functional error, can you spot it?

function Greeter(greeting) {
    this.greeting = greeting;
}

Greeter.prototype.greet = function() {
    return "Hello, " + this.greeting;
};

var greeter = new Greeter({message: "world"});

var button = document.createElement('button');
button.textContent = "Say Hello";
button.addEventListener('click', function(){alert(greeter.greet())});

document.write('Hello from TypeScript!');
document.body.appendChild(document.createElement('br'));
document.body.appendChild(button);
