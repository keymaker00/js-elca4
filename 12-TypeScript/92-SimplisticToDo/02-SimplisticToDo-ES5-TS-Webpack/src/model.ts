'use strict';

var model = {
    todos: [],
    newTodo: {text: '', created: new Date()}
};

export = model;
