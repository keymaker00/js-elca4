'use strict';
var todoListView = {
    init: function () {
        this.todoCountElem = $('#total');
        this.todoListElem = $('#do')

        var $todoListView = $(this);
        this.todoListElem.on('click', '.remove', function (e) {
            var index = $(e.target).parents('li').data('index');
            $todoListView.trigger('item-removed', index);
        });
    },
    render: function (todos) {
        var todo;
        this.todoCountElem.text(todos.length);

        this.todoListElem.html('');
        for (var i = 0, len = todos.length; i < len; i++) {
            todo = todos[i];
            this.todoListElem.append(
                '<li class="clearfix" data-index="' + i + '">' +
                todo.text +
                '<span class="pull-right">' +
                '<button class="btn btn-xs btn-danger remove glyphicon glyphicon-trash"></button>' +
                '</span>' +
                '</li>'
            );
        }
    }
};

export = todoListView;
