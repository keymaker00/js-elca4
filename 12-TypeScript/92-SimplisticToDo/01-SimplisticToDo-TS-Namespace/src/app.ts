'use strict';
namespace ToDo {

    let model = new Model();
    let todoView = new TodoView(); 
    let todoListView = new TodoListView();
    let controller = new Controller(model, todoListView, todoView);
}

