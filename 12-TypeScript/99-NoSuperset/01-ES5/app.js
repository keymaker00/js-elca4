'use strict';

function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
}

Person.prototype.fullName = function(){
    return this.firstName + ' ' + this.lastName;
};

Person.prototype.fullNameReversed = function(){
    return this.lastName + ' ' + this.firstName;
};

var person = new Person('Tyler', 'Durden');
document.write(person.fullName());