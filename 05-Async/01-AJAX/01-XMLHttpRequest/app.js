// See: https://developer.mozilla.org/en/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest

var getBtn = document.getElementById('get');
var postBtn = document.getElementById('post');
var idInput = document.getElementById('id');
var putBtn = document.getElementById('put');
var deleteBtn = document.getElementById('delete');

getBtn.addEventListener('click', getData);
postBtn.addEventListener('click', postData);
putBtn.addEventListener('click', putData);
deleteBtn.addEventListener('click', deleteData);


function getData() {
    var req = new XMLHttpRequest();
    req.addEventListener("load", done);
    req.addEventListener("error", failed); // only fired on network-level events
    req.addEventListener("readystatechange", stateChanged);
    req.open("GET", "http://localhost:3001/comments");
    req.send();

    function stateChanged(){
        if (this.readyState === 4) {
            if (this.status === 200) {
                console.log('success state', this.responseText)
            } else {
                console.log('error state', this.statusText);
            }
        }
    }

    function done() {
        console.log('DONE', this.responseText);
    }

    function failed() {
        console.log('ERROR', this.statusText);
    }
}

function postData() {
    var req = new XMLHttpRequest();
    req.addEventListener("load", done);
    req.open("POST", "http://localhost:3001/comments");
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify({text: 'test - ' + new Date()}));

    function done() {
        console.log('POSTED!');
    }
}

function putData() {

    var id = idInput.value;

    var req = new XMLHttpRequest();
    req.addEventListener("load", done);
    req.open("PUT", "http://localhost:3001/comments/" + id);
    req.setRequestHeader("Content-type", "application/json");
    req.send(JSON.stringify({text: 'test - ' + new Date()}));

    function done() {
        console.log('PUT!');
    }
}

function deleteData() {

    var id = idInput.value;

    var req = new XMLHttpRequest();
    req.addEventListener("load", done);
    req.open("DELETE", "http://localhost:3001/comments/" + id);
    req.send();

    function done() {
        console.log('DELETED!');
    }
}
