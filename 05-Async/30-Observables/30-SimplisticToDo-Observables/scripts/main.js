(function () {
    'use strict';
    const Observable = Rx.Observable;
    const Subject = Rx.Subject;

    const $input = $('#input');
    const $addBtn = $('#addBtn');
    const $do = $('#do');

    const addClicks$ = Observable.fromEvent($addBtn, 'click')
        .map(value => ({type: 'ADD', value: $input.val()}));

    const removeClicks$ = new Subject()
        .map(value => ({type: 'REMOVE', value}));
    // removeClicks.subscribe((todo) => console.log(todo));

    Observable.merge(addClicks$, removeClicks$)
        .startWith([])
        .do(v => console.log(v))
        .scan((d, v) => {
            if (v.type === 'ADD') {
                const text = v.value;
                return [...d, {text}];
            }
            else if (v.type === 'REMOVE') {
                return d.filter(e => e !== v.value)
            }
        })
        // .do(todos => console.log(todos))
        .subscribe((todos) => {
            $input.val('');
            updateTotal(todos);
            renderToDos(todos);
        });


    function updateTotal(todos) {
        $('#total').text(todos.length);
    }

    function renderToDos(todos) {

        $do.html('');
        for (var i = 0, len = todos.length; i < len; i++) {
            var todo = todos[i];

            var li = document.createElement('li');
            li.setAttribute('data-index', i);
            li.classList.add('clearfix');
            li.textContent = todo.text;
            li.addEventListener('click', (function (todo) {
                return function () {
                    removeClicks$.next(todo);
                }
            })(todo));


            $do.append(li);
        }
    }


})();
