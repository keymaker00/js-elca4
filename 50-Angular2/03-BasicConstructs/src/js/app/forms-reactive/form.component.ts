import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";


@Component({
    moduleId: module.id,
    selector: 'aw-form',
    templateUrl: 'form.component.html'
})
export class Form3Component {
    private theForm: FormGroup;

    constructor(formBuilder: FormBuilder){
        this.theForm = formBuilder.group({
            'name': [''],
            'password': ['']
        });
    }

    onSubmit(value: string): void {
        console.log('submitted: ', value, this.theForm.value);
    }

}
