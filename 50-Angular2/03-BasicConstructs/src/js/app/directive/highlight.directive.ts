import {Directive, ElementRef, Input, HostListener, OnInit} from '@angular/core';

@Directive({ selector: '[awHighlight]' })
export class HighlightDirective /*implements OnInit*/ {

    private defaultColor = 'yellow';
    // @Input('awHighlight') highlightColor: string;

    constructor(private el: ElementRef) {
        this.highlight(this.defaultColor);
    }

    private highlight(color: string) {
        this.el.nativeElement.style.backgroundColor = color;
    }

    // ngOnInit(): void {
    //     this.highlight(this.highlightColor || this.defaultColor);
    // }

    // @HostListener('mouseenter') onMouseEnter() {
    //     this.highlight(this.defaultColor);
    // }
    // @HostListener('mouseleave') onMouseLeave() {
    //     this.highlight(null);
    // }

}