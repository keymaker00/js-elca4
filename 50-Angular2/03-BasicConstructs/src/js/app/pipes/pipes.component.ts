import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'aw-pipes',
    templateUrl: 'pipes.component.html',
})
export class PipesComponent {
    message: string = 'Hello from Angular!';
    time: Date = new Date();
    count: number = 0.45699;
}
