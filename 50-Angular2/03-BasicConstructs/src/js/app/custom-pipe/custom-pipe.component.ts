import {Component} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'aw-custom-pipe',
    templateUrl: 'custom-pipe.component.html',
})
export class CustomPipeComponent {
    message:string = 'Hello from angular!';
}

