import {Component} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'aw-databinding',
  templateUrl: 'databinding.component.html',
})
export class DatabindingComponent {

  name:string = 'Angular';
  message1: string = 'You are';
  message2: string = 'a great framework!';

  color:string = 'green';

  onChange(event: UIEvent) {
    this.name = (event.target as HTMLInputElement).value;
  }

  onChange2(value: string) {
    this.name = value;
  }
}
