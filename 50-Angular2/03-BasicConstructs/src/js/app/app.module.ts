import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import {routing} from './app.routing';
import {AdvancedModule} from "./advanced/advanced.module";
import {AppComponent} from './app.component';
import {DatabindingComponent} from "./databinding/databinding.component";
import {FormComponent} from "./forms/form.component";
import {StructuralDirectivesComponent} from "./structural-directives/structural-directives.component";
import {DependencyInjectionComponent} from "./dependency-injection/dependency-injection.component";
import {DiChildComponent} from "./dependency-injection/child.component";
import {ParentComponent} from "./nested-components/parent.component";
import {ChildFormComponent} from "./nested-components/child-form.component";
import {ChildListComponent} from "./nested-components/child-list.component";
import {PipesComponent} from "./pipes/pipes.component";
import {CustomPipeComponent} from "./custom-pipe/custom-pipe.component";
import {CamelCasePipe} from "./custom-pipe/camel-case.pipe";
import {Form2Component} from "./forms2/form2.component";
import {Form3Component} from "./forms-reactive/form.component";
import {AttributeDirectiveComponent} from "./directive/directive.component";
import {HighlightDirective} from "./directive/highlight.directive";
import {DummyComponent} from "./directive/dummy.component";



@NgModule({
    providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        JsonpModule,
        routing,
        AdvancedModule
        //BackendModule is lazy-loaded
    ],
    declarations: [
        AppComponent,
        DatabindingComponent, FormComponent, Form2Component, Form3Component,
        StructuralDirectivesComponent,
        AttributeDirectiveComponent, HighlightDirective,
        PipesComponent, CustomPipeComponent, CamelCasePipe,
        ParentComponent, ChildFormComponent, ChildListComponent,
        DependencyInjectionComponent, DiChildComponent,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
