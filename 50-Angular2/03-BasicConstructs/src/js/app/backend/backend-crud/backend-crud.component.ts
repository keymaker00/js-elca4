import {Component} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

const backendUrl = 'http://localhost:3456/courserater/rest/ratings';

@Component({
    moduleId: module.id,
    selector: 'aw-backend-crud',
    templateUrl: 'backend-crud.component.html'
})
export class BackendCrudComponent {

    ratings:Observable<any>;
    private ratingId:number;
    private participantName:string;
    private grade:number;

    constructor(private _http:Http) {
    }


    getRatings() {
        this.ratings = this._http
            .get(backendUrl)
            .map(r => r.json());
    }

    postRating() {

        const rating = {name: this.participantName, grade: this.grade};

        const headers = new Headers({'Content-Type': 'application/json'});
        const options = new RequestOptions({headers: headers});

        this._http
            .post(backendUrl, JSON.stringify(rating), options)
            .subscribe();
    }

    putRating() {

        const rating = {name: this.participantName, grade: this.grade};

        const headers = new Headers({'Content-Type': 'application/json'});
        const options = new RequestOptions({headers: headers});

        this._http
            .put(`${backendUrl}/${this.ratingId}`, JSON.stringify(rating), options)
            .subscribe();
    }

    deleteRating() {
        this._http
            .delete(`${backendUrl}/${this.ratingId}`)
            .subscribe();
    }

}
