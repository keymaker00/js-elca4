import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BackendAccessComponent } from "./backend-access/backend-access.component";
import { BackendCrudComponent } from "./backend-crud/backend-crud.component";
import { BackendSearchComponent } from "./backend-search/backend-search.component";


const appRoutes: Routes = [

    { path: 'backend-access', component: BackendAccessComponent },
    { path: 'backend-crud', component: BackendCrudComponent},
    { path: 'backend-search', component: BackendSearchComponent},

    { path: '**', redirectTo: 'backend-access' }, // default route
];

export const routing: ModuleWithProviders = RouterModule.forChild(appRoutes);
