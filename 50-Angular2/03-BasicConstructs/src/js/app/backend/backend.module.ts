import {CommonModule} from "@angular/common";
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';

import {BackendAccessComponent} from "./backend-access/backend-access.component";
import {BackendCrudComponent} from "./backend-crud/backend-crud.component";
import {BackendSearchComponent} from "./backend-search/backend-search.component";
import {routing} from "./backend.routing";

@NgModule({
    imports: [CommonModule, FormsModule, HttpModule, JsonpModule, routing], // do not import BrowserModule in a lazy-loaded module
    declarations: [BackendAccessComponent, BackendCrudComponent, BackendSearchComponent]
    // no need to export in a a lazy-loaded module
})
export class BackendModule {}
