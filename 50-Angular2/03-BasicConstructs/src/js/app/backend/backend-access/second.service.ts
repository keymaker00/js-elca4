import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class SecondService {
    constructor(private _http:Http) {}

    getData = () => this._http.get('https://api.github.com/repos/angular/angular/commits')
        .toPromise()
        .then(this.extractData)
        .catch(this.handleError);

    private extractData(res: Response) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Bad response status: ' + res.status);
        }
        let commits = res.json();
        return commits.map(c => ({name: c.commit.author.name, date: new Date(c.commit.author.date)}));
    }

    private handleError (error: any) {
        let errMsg = error.message || 'Server error';
        console.error(errMsg); // log to console instead
        return Promise.reject(errMsg);
    }

}
