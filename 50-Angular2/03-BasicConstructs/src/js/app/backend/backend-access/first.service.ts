import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class FirstService {
    constructor(private _http: Http) { }

    getData = () => this._http.get('https://api.github.com/repos/angular/angular/commits')
        .map(r => r.json().map(c => ({ name: c.commit.author.name, date: new Date(c.commit.author.date) })))
        .catch(this.handleError);

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
