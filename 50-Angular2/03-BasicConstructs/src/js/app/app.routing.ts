import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {DatabindingComponent} from "./databinding/databinding.component";
import {FormComponent} from "./forms/form.component";
import {Form2Component} from "./forms2/form2.component";
import {Form3Component} from "./forms-reactive/form.component";
import {StructuralDirectivesComponent} from "./structural-directives/structural-directives.component";
import {AttributeDirectiveComponent} from "./directive/directive.component";
import {DependencyInjectionComponent} from "./dependency-injection/dependency-injection.component";
import {ParentComponent} from "./nested-components/parent.component";
import {ContentMainComponent} from "./advanced/nested-content/main.component";
import {PipesComponent} from "./pipes/pipes.component";
import {CustomPipeComponent} from "./custom-pipe/custom-pipe.component";
import {LifecycleParentComponent} from "./advanced/lifecycle/lifecycle-parent.component";
import {StylingContainerComponent} from "./advanced/styling/container.component";

const appRoutes: Routes = [
    {path: 'databinding', component: DatabindingComponent},
    {path: 'form', component: FormComponent},
    {path: 'form2', component: Form2Component},
    {path: 'form3', component: Form3Component},
    {path: 'pipes', component: PipesComponent},
    {path: 'structural-directives', component: StructuralDirectivesComponent},
    {path: 'attribute-directive', component: AttributeDirectiveComponent},
    {path: 'custom-pipe', component: CustomPipeComponent},
    {path: 'styling', component: StylingContainerComponent},
    {path: 'nested-components', component: ParentComponent},
    {path: 'nested-content', component: ContentMainComponent},
    {path: 'dependency-injection', component: DependencyInjectionComponent},
    {path: 'lifecycle', component: LifecycleParentComponent},
    {path: 'backend', loadChildren: 'src/js/app/backend/backend.module#BackendModule'},

    {path: '**', redirectTo: 'databinding'}, // default route
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
