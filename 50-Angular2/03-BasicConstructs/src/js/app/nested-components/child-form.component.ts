import {Component, Output, EventEmitter} from '@angular/core';
import {IPerson} from "./parent.component";

@Component({
    moduleId: module.id,
    selector: 'aw-child-form',
    templateUrl: 'child-form.component.html',
})
export class ChildFormComponent {
    private firstName:string;
    private lastName:string;
    private age:number;

    private addCount:number = 0;

    @Output() onAddPerson = new EventEmitter<IPerson>();

    addPerson() {
        this.onAddPerson.emit({firstName: this.firstName, lastName: this.lastName, age: this.age});
        this.addCount++;
    }

    reset() {
        this.addCount = 0;
    }
}
