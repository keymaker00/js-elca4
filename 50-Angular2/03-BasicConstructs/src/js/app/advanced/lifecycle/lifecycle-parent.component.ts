import {Component}         from '@angular/core';
import {LoggerService}  from './logger.service';

@Component({
    moduleId: module.id,
    selector: 'aw-lifecycle-parent',
    templateUrl: 'lifecycle-parent.component.html',
    providers: [LoggerService]
})
export class LifecycleParentComponent {

    hasChild = false;
    hookLog:string[];

    theDate = new Date();
    private _logger:LoggerService;

    constructor(logger:LoggerService){
        this._logger = logger;
        this.hookLog = logger.logs;
    }

    toggleChild() {
        this.hasChild = !this.hasChild;
        if (this.hasChild) {
            this.theDate = new Date();
            this._logger.clear(); // clear log on create
        }
        this._logger.tick();
    }

    update() {
        this.theDate = new Date();
        this._logger.tick();
    }
}
