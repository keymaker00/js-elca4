import {Component} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'aw-content-main',
  templateUrl: 'main.component.html',
})
export class ContentMainComponent {}

