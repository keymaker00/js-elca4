import {Component} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'aw-unstyled',
  template: '<button>Unstyled</button>',
})
export class UnstyledComponent {}

