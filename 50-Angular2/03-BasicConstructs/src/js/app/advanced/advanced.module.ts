import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { LifecycleParentComponent } from "./lifecycle/lifecycle-parent.component";
import { LifecycleComponent } from "./lifecycle/lifecycle.component";
import { StylingContainerComponent } from "./styling/container.component";
import { StylingComponent } from "./styling/styling.component";
import { UnstyledComponent } from "./styling/unstyled.component";
import {ClockComponent} from "./nested-content/clock.component";
import {BlackBoxComponent} from "./nested-content/black-box.componen";
import {ContentMainComponent} from "./nested-content/main.component";


@NgModule({
    imports: [BrowserModule, FormsModule],
    declarations: [
        LifecycleParentComponent, LifecycleComponent,
        StylingContainerComponent, StylingComponent, UnstyledComponent,
        ContentMainComponent, BlackBoxComponent, ClockComponent
        ],
    exports: [
        LifecycleParentComponent, LifecycleComponent,
        StylingContainerComponent, StylingComponent, UnstyledComponent,
        ContentMainComponent, BlackBoxComponent, ClockComponent
        ],
})
export class AdvancedModule { }
