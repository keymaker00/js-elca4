import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'aw-form2',
    templateUrl: 'form2.component.html',
    // styleUrls:['form2.component.css']
})
export class Form2Component implements AfterViewInit{
    @ViewChild('formRef') form;
    username = "Jonas";

    ngAfterViewInit(): void {
        this.form.valueChanges
            .subscribe(v => console.log(v));
    }

    onSubmit(value){
        console.log(value);
    }



}
