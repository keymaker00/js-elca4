import 'jquery'; // needed for bootstrap
import 'bootstrap'; 
import { platformBrowserDynamic }    from '@angular/platform-browser-dynamic';
import {AppModule} from './app/app.module';

// import {enableProdMode} from '@angular/core';
// enableProdMode();

platformBrowserDynamic().bootstrapModule(AppModule);
