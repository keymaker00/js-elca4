/**
 * System configuration for Angular 2 samples
 * Adjust as necessary for your application needs.
 */
(function (global) {

    // map tells the System loader where to look for things
    var map = {
        'jquery': 'node_modules/jquery',
        'bootstrap': 'node_modules/bootstrap',
        'rxjs': 'node_modules/rxjs',
        '@angular': 'node_modules/@angular',
        'ts': 'node_modules/plugin-typescript/lib',
        'typescript': 'node_modules/typescript/lib/typescript.js',
        'src/js': 'src/js',
    };

    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        'src/js': {main: 'main.ts', defaultExtension: 'ts'},
        'ts': {main: 'plugin.js', defaultExtension: 'js'}
    };

    var packageNames = [
        'core',
        'common',
        'compiler',
        'platform-browser',
        'platform-browser-dynamic',
        'http',
        'router',
        'forms'
    ];

    // add package entries for angular packages
    packageNames.forEach(function (pkgName) {
        // Bundled (~40 requests):
        packages['@angular/' + pkgName] = {main: '/bundles/' + pkgName + '.umd.js', defaultExtension: 'js'};

        // Individual files (~300 requests):
        // packages['@angular/'+pkgName] = { main: 'index.js', defaultExtension: 'js' };
    });

    packages['jquery'] = {main: '/dist/jquery.min.js', defaultExtension: 'js'};
    packages['bootstrap'] = {main: '/dist/js/bootstrap.min.js', defaultExtension: 'js'};

    // How to map rxjs so that we do not get a gazillion requests????
    packages['rxjs'] = {main: '/bundles/Rx.min.js', defaultExtension: 'js'};

    var config = {
        transpiler: 'ts',
        typescriptOptions: {
            tsconfig: true
        },
        meta: {
            'typescript': {
                "exports": "ts"
            }
        },
        map: map,
        packages: packages,
    };

    System.config(config);

})(this);
