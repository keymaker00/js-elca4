import {Component, Input} from '@angular/core';

@Component({
    selector: 'cr-table-counter',
    template: '<h3>Count: {{count}}</h3>',
})
export class TableCounterComponent {
    @Input() count;
}

