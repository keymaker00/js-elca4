import {Component, ViewChild} from '@angular/core';
import {Rating} from '../../model/rating.model';

@Component({
    selector: 'cr-ratings',
    templateUrl: './ratings.component.html'
})
export class RatingsComponent {

    private name;
    private grade;

    // rating = new Rating();
    ratings: Array<Rating> = [];
    @ViewChild('formRef') form;

    constructor() {}

    addRating() {
        const r = new Rating();
        r.name = this.name;
        r.grade = this.grade;
        this.ratings.push(r);
        this.name = '';
        this.grade = 0;

        this.form.reset();
    }

    removeRating(rating: Rating) {
        this.ratings.splice(this.ratings.indexOf(rating), 1);
    }
}

