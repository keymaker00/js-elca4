import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Rating} from "../../model/rating.model";

@Component({
  selector: 'cr-rating-list',
  templateUrl: './rating-list.component.html',
})
export class RatingListComponent {
  @Input() ratings;
  @Output() onRemoveRating = new EventEmitter<Rating>();

  removeRating(rating: Rating) {
    this.onRemoveRating.emit(rating);
  }
}
