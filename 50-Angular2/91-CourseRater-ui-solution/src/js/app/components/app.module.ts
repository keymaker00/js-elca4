import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { routing } from './main.routes';
import { Main }  from './main.component';
import {RatingsComponent} from './ratings/ratings.component'
import {RatingListComponent} from './ratings/rating-list.component'
import {TableCounterComponent} from './ratings/table-counter.component'
import {UsersComponent} from './users/users.component'

@NgModule({
    providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
    imports: [BrowserModule, FormsModule, HttpModule, routing],
    declarations: [Main, RatingsComponent, RatingListComponent, TableCounterComponent, UsersComponent],
    bootstrap: [Main]
})
export class AppModule { }