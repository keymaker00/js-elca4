import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// import { RatingsRoutes } from './ratings/ratings.routes';
// import {UsersRoutes} from './users/users.routes';
import {RatingsComponent} from "./ratings/ratings.component";
import {UsersComponent} from "./users/users.component";

const appRoutes: Routes = [

    // Alternative for "decentralized routing configuration
    // ...RatingsRoutes,
    // ...UsersRoutes,

    {path: 'ratings', component: RatingsComponent},
    {path: 'users', component: UsersComponent},
    {path: '**', redirectTo: 'ratings'}, // default route
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
