// Webpack infrastructure
import 'style-loader/addStyles.js';

// Polyfills
import 'core-js';
import 'reflect-metadata';
// import 'zone.js'; // Typescript compiler has an error
// Workaround
import 'zone.js/dist/zone';
import 'zone.js/dist/long-stack-trace-zone';


// Angular
import '@angular/common';
import '@angular/core';
import '@angular/compiler';
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/forms';
// import '@angular/http';
// import '@angular/router';

// import 'rxjs/Rx';
