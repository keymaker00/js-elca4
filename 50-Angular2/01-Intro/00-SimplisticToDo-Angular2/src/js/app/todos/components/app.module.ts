import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule}   from '@angular/forms';

import {TodoApp}  from './app.component';
import {NewTodo} from './new-todo/new-todo.component'
import {ToDoList} from './todo-list/todo-list.component';

@NgModule({
    imports: [BrowserModule, FormsModule],
    declarations: [TodoApp, NewTodo, ToDoList],
    bootstrap: [TodoApp]
})
export class AppModule {}
