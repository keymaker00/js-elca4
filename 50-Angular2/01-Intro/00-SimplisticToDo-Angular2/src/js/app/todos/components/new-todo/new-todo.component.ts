import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {ToDo} from "../../model/todo.model";

@Component({
    selector: 'td-new-todo',
    templateUrl: './new-todo.component.html'
})
export class NewTodo {

    private newToDo = new ToDo();
    @Output() onAddToDo = new EventEmitter<ToDo>();

    addToDo(rating) :void {
        this.onAddToDo.emit(this.newToDo);
        this.newToDo = new ToDo();
    }
}
