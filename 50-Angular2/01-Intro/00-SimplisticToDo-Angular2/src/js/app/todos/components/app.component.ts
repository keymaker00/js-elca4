import {Component, OnInit} from '@angular/core';

import {ToDo} from "../model/todo.model";
import {ToDoService} from "../model/todo.service";

@Component({
    selector: 'td-todo-app',
    templateUrl: './app.component.html',
    providers: [ToDoService]
})
export class TodoApp implements OnInit {

    todos: Array<ToDo> = [];

    constructor(private todoService:ToDoService){}

    ngOnInit() {
        this.todos = this.todoService.loadToDos();
    }

    addToDo(todo: ToDo){
        this.todos.push(todo);
        this.todoService.saveToDos(this.todos);
    }

    removeToDo(todo: ToDo){
        this.todos.splice(this.todos.indexOf(todo),1);
        this.todoService.saveToDos(this.todos);
    }
}
