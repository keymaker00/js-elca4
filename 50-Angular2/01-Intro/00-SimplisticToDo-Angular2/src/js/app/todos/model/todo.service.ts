import {Injectable} from "@angular/core";
import {ToDo} from "./todo.model";

const TODOS_KEY = 'TODO_APP.todos';

@Injectable()
export class ToDoService {

    loadToDos():Array<ToDo> {
        var loadedToDos = JSON.parse(localStorage.getItem(TODOS_KEY));
        return loadedToDos || [];
    }

    saveToDos(todos:Array<ToDo>):void {
        localStorage.setItem(TODOS_KEY, JSON.stringify(todos));
    }
}
