export class ToDo {
    title: string;
    completed: boolean;
    entered: Date;

    constructor() {
        this.entered = new Date();
    }
}
