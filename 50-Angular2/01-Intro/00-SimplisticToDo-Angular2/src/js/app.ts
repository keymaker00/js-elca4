import '../css/index.css'
import { platformBrowserDynamic }    from '@angular/platform-browser-dynamic';
import { AppModule } from './app/todos/components/app.module';

// import {enableProdMode} from '@angular/core';
// enableProdMode();

console.log('Bootstrapping ...');
platformBrowserDynamic().bootstrapModule(AppModule);
