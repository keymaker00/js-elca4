/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GreeterComponent } from './greeter.component';
import {FormsModule} from "@angular/forms";

describe('GreeterComponent', () => {
  let component: GreeterComponent;
  let fixture: ComponentFixture<GreeterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GreeterComponent ],
      imports: [FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GreeterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the name', () => {
    fixture = TestBed.createComponent(GreeterComponent);
    fixture.componentInstance.name = 'John';

    fixture.detectChanges();

    const html = fixture.debugElement.nativeElement;
    expect(html.querySelector('h2').textContent).toContain('Hello John!');
  })
});
