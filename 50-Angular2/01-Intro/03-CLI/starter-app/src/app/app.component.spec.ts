/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {GreeterComponent} from "./greeter/greeter.component";
import {FormsModule} from "@angular/forms";

describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, GreeterComponent
      ],
      imports: [FormsModule]
    });
  });

  it('should create the app', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app works!'`, async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app works!');
  }));

  it('should render title in a h1 tag', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('app works!');
  }));

  it('should display the greeter', async(() => {
    let fixture = TestBed.createComponent(AppComponent);

    // console.log('DOM Children: ', fixture.debugElement.children[1]);
    let greeterInstance: GreeterComponent = <GreeterComponent>fixture.debugElement.children[ 1 ].componentInstance;
    greeterInstance.name = 'Jonas';

    fixture.detectChanges();


    expect(greeterInstance).toBeTruthy();
    expect(greeterInstance.name).toBe('Jonas');
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Hello Jonas!');

  }));

});
