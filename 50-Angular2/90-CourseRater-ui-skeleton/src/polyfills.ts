// Polyfills
// These modules are what's in angular 2 bundle polyfills so don't include them
// import 'es6-shim';
// import 'es6-promise';

// Alternative: CoreJS has all the polyfills you need
import 'core-js';

// import 'zone.js'; // has an error
// Workaround:
import 'zone.js/dist/zone';
import 'zone.js/dist/long-stack-trace-zone';

import 'reflect-metadata';
