import { RatingsComponent } from './ratings.component';


export const RatingsRoutes = [
    {
        path: 'ratings',
        component: RatingsComponent
    }
];
