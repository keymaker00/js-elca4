export class Rating {
    name;
    grade;
    entered;

    constructor() {
        this.entered = new Date();
    }
}
