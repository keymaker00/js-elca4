var webpack = require('webpack');
var path = require('path');
var UglifyJsPlugin = require('webpack/lib/optimize/UglifyJsPlugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');


var webpackConfig = {
  entry: {
    'polyfills': './src/polyfills.ts',
    'vendor':    './src/vendor.ts',
    'app':       './src/app.ts'
  },
  output: {
    path: './dist',
    filename: 'app/[name].bundle.js',
    sourceMapFilename: 'app/[name].map',
    chunkFilename: 'app/[id].chunk.js'
  },
  resolve: {
    root: [ path.join(__dirname, 'src') ],
    extensions: ['', '.ts', '.js']
  },
  module: {
    loaders: [
      { test: /\.ts$/, loaders: ['ts-loader', 'angular2-template-loader'] },
      { test: /\.html$/, loader: 'raw-loader' },
      {test: /\.css$/,  loader: ['to-string-loader', 'css-loader']},
      {test: /\.less$/, loader: 'style!css!less', exclude: /node_modules/},
      {test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/, loader : 'file-loader'},
    ]
    // preLoaders: [
    //   { test: /\.ts$/, loader: 'tslint-loader', exclude: [ 'node_modules'] }
    // ]
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(true),
    new webpack.ProvidePlugin({ jQuery: "jquery" }), // make jQuery global for bootstrap
    new webpack.optimize.CommonsChunkPlugin({ name: ['app', 'vendor', 'polyfills'], minChunks: Infinity }),
    new HtmlWebpackPlugin({template: './src/index.html', chunksSortMode: packageSort(['polyfills', 'vendor', 'app'])})
  ],
  tslint: {
    emitErrors: true,
    failOnHint: false,
    resourcePath: 'src'
  },
  devtool: 'source-map',
  devServer: {
    historyApiFallback: true,
    devServer: {
      contentBase: "./app"
    }
  }
};

module.exports = webpackConfig;


function packageSort(packages) {
  return function sort(left, right) {
    var leftIndex = packages.indexOf(left.names[0]);
    var rightindex = packages.indexOf(right.names[0]);

    if ( leftIndex < 0 || rightindex < 0) {
      throw "unknown package";
    }

    if (leftIndex > rightindex){
      return 1;
    }

    return -1;
  }
};
