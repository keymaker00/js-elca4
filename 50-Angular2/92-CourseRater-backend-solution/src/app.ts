import './css/courserater.less'
import { platformBrowserDynamic }    from '@angular/platform-browser-dynamic';
import {AppModule} from './js/app/components/app.module';

import 'rxjs/Rx';

// import {enableProdMode} from '@angular/core';
// enableProdMode();

console.log('Bootstrapping ...');
platformBrowserDynamic().bootstrapModule(AppModule);


