// Vendors
// import 'bootstrap';
// import "~bootstrap/less/bootstrap";

import './css/bootstrap.less'

// Angular 2
import '@angular/common';
import '@angular/compiler';
import '@angular/core';
import '@angular/forms';
import '@angular/http';
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/router';


// RxJS 5
import "rxjs/Observable";
import 'rxjs/Rx';


// For vendors for example jQuery, Lodash, angular2-jwt import them here
// Also see src/typings.d.ts as you also need to run `typings install x` where `x` is your module
