export class Rating {
    id;
    name;
    grade;
    entered;

    constructor() {
        this.entered = new Date();
    }

    static fromData(ratingData) :Rating {
        const rating = new Rating();

        rating.id = ratingData.id;
        rating.name = ratingData.name;
        rating.grade = ratingData.grade;
        rating.entered = new Date(ratingData.entered);

        return rating;
    }
}
