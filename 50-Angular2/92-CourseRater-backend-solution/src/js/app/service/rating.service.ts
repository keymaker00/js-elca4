import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Rating} from '../model/rating.model';
import 'rxjs/Rx'; // Note: we should only include the operators we need to tune package size
import {Observable} from 'rxjs/Observable';

const backendUrl = 'http://localhost:3456/courserater/rest/ratings';

@Injectable()
export class RatingService {
    constructor(private _http: Http) {}

    getRatings() : Observable<Rating[]> {
        return this._http.get(backendUrl)
            .map(this.extractData)
            .map((ratingsData) => ratingsData.map((r) => Rating.fromData(r)))
            .catch(this.handleError);
    }

    saveRating(rating: Rating) : Observable<Rating> {
        const body = JSON.stringify(rating);
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this._http.post(backendUrl, body, options)
            .map(this.extractData)
            .map(Rating.fromData)
            .catch(this.handleError);
    }

    deleteRating(rating: Rating) : Observable<any> {
        return this._http.delete(`${backendUrl}/${rating.id}`);
    }

    private extractData(res: Response) {
        if (res.status < 200 || res.status >= 300) {
            throw new Error('Bad response status: ' + res.status);
        }
        let body = res.json();
        return body.data || {};
    }

    private handleError(error: any) {
        let errMsg = error.message || 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}

