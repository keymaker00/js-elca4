import {Component, OnInit} from '@angular/core';
import {RatingService} from "../service/rating.service";

@Component({
    selector: 'app',
    templateUrl: './main.component.html',
    providers: [RatingService]
})
export class Main { }
