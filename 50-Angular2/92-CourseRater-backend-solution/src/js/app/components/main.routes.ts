import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RatingsRoutes } from './ratings/ratings.routes';
import {UsersRoutes} from './users/users.routes';

const appRoutes: Routes = [
    ...RatingsRoutes,
    ...UsersRoutes,

    { path: '**', redirectTo: 'ratings' }, // default route
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
