import {Component, Input} from '@angular/core';

@Component({
    selector: 'cr-table-counter',
    template: '<h3>Count: {{collection.length}}</h3>',
})
export class TableCounterComponent {
    @Input() collection;
}

