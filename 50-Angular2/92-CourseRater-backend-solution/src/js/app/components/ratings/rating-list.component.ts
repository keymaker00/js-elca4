import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'cr-rating-list',
  templateUrl: './rating-list.component.html',
})
export class RatingListComponent {
  @Input() ratings;
  @Output() onRemoveRating = new EventEmitter();

  removeRating(rating) {
    this.onRemoveRating.emit(rating);
  }
}
