import {Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {Rating} from '../../model/rating.model';
import {RatingService} from "../../service/rating.service";
import {Observable} from "rxjs/Rx";

@Component({
    selector: 'cr-ratings',
    templateUrl: './ratings.component.html',
})
export class RatingsComponent implements OnInit {
    rating = new Rating();
    ratings : Rating[] = [];

    constructor(private _ratingService: RatingService) {}

    ngOnInit() {
        // Note that the request is not executed when there is no subscription
        this._ratingService.getRatings()
            .subscribe(
                ratings => {
                    this.ratings = ratings;
                },
                this.handleError
            );
    }

    addRating() {
        this._ratingService.saveRating(this.rating)
            .subscribe(
                rating => this.ratings.push(rating),
                this.handleError
            );
        this.rating = new Rating();
    }

    removeRating(rating) {
        this._ratingService.deleteRating(rating)
            .subscribe(
                () => this.ratings.splice(this.ratings.indexOf(rating), 1),
                this.handleError
            );
    }

    private handleError(error) {
        let errMsg = error.message || 'Error calling server';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}

