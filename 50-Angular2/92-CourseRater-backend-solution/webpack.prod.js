var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var path = require('path');
var UglifyJsPlugin = require('webpack/lib/optimize/UglifyJsPlugin');
var webpackConfig = require('./webpack.config');


var webpackProdConfig = {
  plugins: [
    new UglifyJsPlugin({beautify: false, mangle: {screw_ie8: true}, comments: false })
  ]
};

module.exports = webpackMerge(webpackConfig, webpackProdConfig);
