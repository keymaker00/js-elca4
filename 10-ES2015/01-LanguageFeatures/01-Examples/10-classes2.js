class Person {
    constructor(firstName, lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    fullName() { return this.firstName + ' ' + this.lastName};
}

const pers = new Person('John', 'Doe');
console.log(pers.fullName());
console.log(typeof Person);

// class Employee extends Person{
//     constructor(employer, firstName, lastName){
//         super(firstName, lastName);
//         this._employer = employer;
//     }
//
//     get employer() {
//         return this._employer;
//     }
// }
//
// const empl = new Employee('Club Mayhem', 'Tyler', 'Durden');
// console.log(empl.fullName());
// console.log(empl.employer);
