function* range(start, end, step) {
    while (start < end) {
        yield start;
        start += step;
    }
}

console.log('Generator:' + range(0, 10, 2));
console.log('Generator first value:' + range(0, 10, 2).next().value);

for (let i of range(0, 10, 2)) {
    console.log(i);
}


//------------
function* greet(){
    console.log(`You called 'next()'`);
    yield "hello";
}

let greeter = greet();
console.log(greeter);
let next = greeter.next();
console.log(next);
let done = greeter.next();
console.log(done);

for (let v of greet()) {
    console.log(v);
}


//------------
function* greet2(){
    let friendly = yield "How";
    friendly = yield friendly + "are";
    yield friendly + "you?";
}

let greeter2 = greet2();
console.log(greeter2.next().value);
console.log(greeter2.next(" the heck ").value);
console.log(greeter2.next(" silly ol'").value);
