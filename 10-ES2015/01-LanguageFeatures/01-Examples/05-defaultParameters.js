function greet(message, name = 'Joe'){
    console.log(message + ', ' + name);
}

greet();
greet('Hello');
greet('Goodbye', 'Jeff');


function run(callback = () => console.log('Wow!')){
    callback();
}

run(function(){
    console.log('Hey!');
});

run();
