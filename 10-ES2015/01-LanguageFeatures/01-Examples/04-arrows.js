//ES5
var greet1 = function(message, name){
    return message + name;
};
console.log(greet1('Hello', 'John'));

//ES6
const greet2 = (message,name) => {return message + name};
//var greetingArrow = (message,name) => message + name;
console.log( greet2('Hello', 'John'));

// // Arrow Functions
// let square = x => x * x;
// let add = (a, b) => a + b;
// let pi = () => 3.1415;
//
// console.log(square(5));
// console.log(add(3, 4));
// console.log(pi());


// const names = ['John', 'Jane', 'Doe'];
// names.forEach(n => console.log(n));


// function Controller1(){
//     this.count = 0;
//     this.increment = function(){
//         this.count++;
//         console.log('Count:' + this.count);
//     };
// }
// var ctrl = new Controller1();
// setTimeout(ctrl.increment, 0);
//
//
// function Controller2(){
//     this.count = 0;
//     this.increment = () => {
//         this.count++;
//         console.log('Count:' + this.count);
//     };
// }
// var ctrl2 = new Controller2();
// setTimeout(ctrl2.increment, 0);


//
// //ES5
// var rob = {
//     _name: "Bob",
//     _friends: ['John'],
//     printFriends() {
//         var that = this; // capturing this
//         this._friends.forEach(function(f) {
//             console.log(that._name + " knows " + f); // this would be the global context
//         });
//     }
// };
// rob.printFriends();
//
// //ES6
// var bob = {
//     _name: "Bob",
//     _friends: ['John'],
//     printFriends() {
//         this._friends.forEach(f =>
//             console.log(this._name + " knows " + f)); // this is lexical scoped
//     }
// };
// bob.printFriends();
//

//
