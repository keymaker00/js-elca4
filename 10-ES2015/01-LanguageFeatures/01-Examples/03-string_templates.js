let person = {name: 'John Smith'};
let tpl1 = `My name is ${person.name}.`;

let tpl2 = `
    My
        name
            is
                ${person.name}
`;

let tpl3 = `My name is ${person.name + new Date()}.`;

console.log(tpl1);
console.log(tpl2);
console.log(tpl3);
