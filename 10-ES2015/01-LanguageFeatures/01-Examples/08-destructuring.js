const array = [1,2];
const [one, two] = array; // array destructuring

const obj = {three: 3, four: 4};
const {three, four, five = 0} = obj; // object destructuring
// const {three:five, four:six} = obj; // renaming


console.log(one);
console.log(two);
console.log(three);
console.log(four);
console.log(five);
// console.log(six);
//
//
// var [first,,,,fifth] = ['one', 'two', 'three', 'four', 'five'];
// console.log(first);
// console.log(fifth);
//
// var persons = [
//     {
//         firstName: "Katniss",
//         lastName: "Everdeen",
//         district: 12
//     },
//     {
//         firstName: "Peeta",
//         lastName: "Mellark",
//         district: 12
//     },
//     {
//         firstName: "Johanna",
//         lastName: "Mason",
//         district: 7
//     },
//     {
//         firstName: "Finnick",
//         lastName: "Odair",
//         district: 4
//     }
// ];
//
// persons.forEach(({district}) => console.log(district));
