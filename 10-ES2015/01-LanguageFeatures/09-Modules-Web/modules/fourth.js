export let greetingMessage = 'Greetings from 4th module';

export function greet() {
    console.log(greetingMessage);
}

export let greeter =  {
    greetingMessage,
    greet
};
