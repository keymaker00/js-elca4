module.exports = function () {
    return {
        restrict: 'E',
        template: '<h3>Count: {{collection.length}}</h3>',
        scope: {collection: '='}
    }
};

