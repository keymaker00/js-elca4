lesson("about destructuring in ES6", () => {

    learn("how an array can be destructured", () => {

        const arr = [1,2];

        // The following statement declares two variables
        const [one, two] = arr;

        expect(one).toBe(FILL_ME_IN);
        expect(two).toBe(FILL_ME_IN);
    });

    learn("how an object can be destructured", () => {

        const obj = {three: 3, four: 4 , five: 5, six: 6};

        // The following statement declares three variables
        const {three, four, five} = obj;

        expect(three).toBe(FILL_ME_IN);
        expect(four).toBe(FILL_ME_IN);
        expect(five).toBe(FILL_ME_IN);
    });

    learn("how an object destructuring can be combined with default values", () => {

        const obj = {three: 3, four: 4 , five: 5, six: 6};

        const {zero = 0, three = 1, seven = 7} = obj;

        expect(zero).toBe(FILL_ME_IN);
        expect(three).toBe(FILL_ME_IN);
        expect(seven).toBe(FILL_ME_IN);

    });

    learn("how an object destructuring can be used for passing default function parameters", () => {

        const obj = {three: 3, four: 4 , five: 5, six: 6};
        processValues(obj);

        function processValues({zero = 0, three = 1, seven = 7}){
            expect(zero).toBe(FILL_ME_IN);
            expect(three).toBe(FILL_ME_IN);
            expect(seven).toBe(FILL_ME_IN);
        }
    });


    learn("how to select properties out of objects with destructuring", () => {

        const persons = [
            {
                firstName: "Katniss",
                lastName: "Everdeen",
                district: 12
            },
            {
                firstName: "Johanna",
                lastName: "Mason",
                district: 7
            },
            {
                firstName: "Finnick",
                lastName: "Odair",
                district: 4
            }
        ];

        const districts = persons.map(({district}) => district);

        expect(districts.join(',')).toBe(FILL_ME_IN);

    });

    learn("how to accept object properties as function arguments with destructuring", () => {

        const paramObject = { name: 'Katniss', district: 12};

        function createMessage({name, district}){
            return `Hello ${name} from district ${district}!`
        }

        const message = createMessage(paramObject);

        expect(message).toBe(FILL_ME_IN);
    });

        /// NOTE:
    /// The code below only works if you transpile with Babel and the appropriate babel-plugin
    /// In the test-setup this is the case if you select the 'transpile' checkbox

    // learn("how to use rest properties for object destructuring in ES.next", () => {
    //
    //     // Object Rest/Spread Properties ist an ECMAScript language proposal and not yet standardized:
    //     // https://github.com/sebmarkbage/ecmascript-rest-spread
    //     // Babel however allows you to use that feature already
    //
    //     let obj = {three: 3, four: 4 , five: 5, six: 6};
    //     let {three, four, ...rest} = obj;
    //
    //     expect(typeof rest).toBe(FILL_ME_IN);
    //     expect(rest.five).toBe(FILL_ME_IN);
    //     expect(rest.six).toBe(FILL_ME_IN);
    // });

});

