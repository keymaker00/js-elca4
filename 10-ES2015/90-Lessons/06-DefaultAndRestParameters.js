lesson("about default and rest parameters", () => {

    learn("how ES6 supports default parameters", () => {

        function greet(message, name = 'Joe'){
            return `${message} ,${name}`;
        }

        function execute(callback = () => 'Default'){
            return callback();
        }

        expect(greet('Hello')).toBe(FILL_ME_IN);
        expect(greet('Goodbye', 'Jeff')).toBe(FILL_ME_IN);
        expect(greet()).toBe(FILL_ME_IN);
        expect(execute(() => 'Specific')).toBe(FILL_ME_IN);
        expect(execute()).toBe(FILL_ME_IN);
    });

    learn("how in ES5 all arguments are available on the arguments parameter", () => {

        let out = '';
        function logAllArguments(firstArg) {
            out += firstArg;
            for (var i=0; i < arguments.length; i++) { // arguments is not an array (i.e. methods from Array.prototype are not available)
                out += arguments[i];
            }
        }

        logAllArguments(1, 2, 3, 'gugus');

        expect(out).toBe(FILL_ME_IN);
    });

    learn("how in ES6 arguments can be passed with the rest parameter syntax", () => {

        let out = '';
        function logAllArguments(firstArg, ...args) {
            out += firstArg;
            for (let arg of args.slice(1)) { // args is a proper array
                out += arg;
            }
        }

        logAllArguments(1, 2, 3, 'gugus');

        expect(out).toBe(FILL_ME_IN);
    });

});

