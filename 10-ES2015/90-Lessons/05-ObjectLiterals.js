lesson("about object literal enhancements in ES6", () => {

    learn("how object creation bears some duplication in ES5", () => {

        var firstName = 'Jonas';
        var lastName = 'Bandi';
        var greet = function(){return 'Hello';};

        var obj = {
            firstName: firstName,
            lastName: lastName,
            greet: greet
        };

        expect(obj.greet()).toBe(FILL_ME_IN);
    });

    learn("how object creation is even easier in ES6", () => {

        var firstName = 'Jonas';
        var lastName = 'Bandi';
        var greet = function(){return 'Hello';};

        //property names can be omitted
        var obj = {
            firstName,
            lastName,
            greet
        };

        expect(obj.greet()).toBe(FILL_ME_IN);
    });

    learn("a shortcut notation for methods", () => {

        var firstName = 'Jonas';
        var lastName = 'Bandi';

        var obj = {
            firstName,
            lastName,
            greet(){return 'Hello';}
        };

        expect(obj.greet()).toBe(FILL_ME_IN);
    });

    learn("property names can be dynamic", () => {

        var firstName = 'Jonas';
        var lastName = 'Bandi';

        var prop = 'say';
        var obj = {
            firstName,
            lastName,
            [prop]: () => 'Hello'
        };

        expect(obj.say()).toBe(FILL_ME_IN);
    });

});

