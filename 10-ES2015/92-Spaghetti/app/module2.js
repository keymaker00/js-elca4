var MODULE_PREFIX = "module2_";

function storeDate(){
    localStorage.setItem(MODULE_PREFIX + 'val1', new Date().toDateString());
    localStorage.setItem(MODULE_PREFIX + 'val2', new Date().toTimeString());
}

function loadDate() {
    var val1 = localStorage.getItem(MODULE_PREFIX + 'val1');
    var val2 = localStorage.getItem(MODULE_PREFIX + 'val2');

    return [val1, val2];
}

