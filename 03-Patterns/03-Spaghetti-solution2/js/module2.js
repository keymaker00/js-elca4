var module2 = (function() {
    var MODULE_PREFIX = "module2_";

    initSettings();
    //loadSettings();

    return {
        loadSettings: loadSettings
    };

    function initSettings() {
        sessionStorage.setItem(MODULE_PREFIX + 'val1', new Date().toDateString());
        sessionStorage.setItem(MODULE_PREFIX + 'val2', new Date().toTimeString());
    }

    function loadSettings() {
        var val1 = sessionStorage.getItem(MODULE_PREFIX + 'val1');
        var val2 = sessionStorage.getItem(MODULE_PREFIX + 'val2');

        console.log("Loaded setting. Val1:" + val1 + ", Val2:" + val2);
        return [val1, val2];
    }

});