module.exports = function (wallaby) {
    return {
        files: [
            'node_modules/jquery/dist/jquery.min.js',
            'src/app/**/*.js'
        ],

        tests: [
            'src/test/spec/**/*Spec.js'
        ]
    };
};
