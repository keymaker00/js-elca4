/* exported registerButtonHandler */

function registerButtonHandler(){
    'use strict';

    var addBtn = document.getElementById('add-button');
    addBtn.addEventListener('click', addToDo);
}

function addToDo(){
    'use strict';
    var input = document.getElementById('todo-text');

    if (input.value.length > 0) {
        var node = document.createElement('li');
        var textnode = document.createTextNode(input.value);
        node.appendChild(textnode);
        document.getElementById('todo-list').appendChild(node);
        input.value = '';
    }
}
