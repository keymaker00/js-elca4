/* global fitText */

// Note: The code below could be replaced with two lines of jQuery:
//// jQuery('form').on('submit', function(e){e.preventDefault();});
//// jQuery('h1').fitText(0.7);
// But we don't want to use jQuery in the initial example ...

// Disable from submit... we are a SPA!
var forms = document.querySelectorAll('form');
for(var i = 0; i < forms.length; i++){
    attachCancelEventHandler(forms[i]);
}

if (window.fitText) {
    var title = document.getElementById('title');
    fitText(title, 0.8);
}


function attachCancelEventHandler(element){
    'use strict';
    if (window.addEventListener) {
        element.addEventListener('submit', cancelEvent);
    }
    else {
        element.attachEvent('onsubmit', cancelEvent);
    }
}

function cancelEvent(event) {
    'use strict';
    if (event.preventDefault) {
        event.preventDefault();
    } else {
        event.returnValue = false;
    }
}