/*global $, describe, beforeEach, afterEach, it, expect, addToDo, registerButtonHandler */
describe('ToDo List', function () {
    'use strict';

    beforeEach(function () {

        // Programmatically set up dom for test
        document.body.innerHTML = ''; // clean test setup
        var app = document.createElement('div');
        var input = document.createElement('input');
        input.setAttribute('id', 'todo-text');
        var button = document.createElement('button');
        button.setAttribute('id', 'add-button');
        var list = document.createElement('ul');
        list.setAttribute('id', 'todo-list');
        app.appendChild(input);
        app.appendChild(button);
        app.appendChild(list);
        document.body.appendChild(app);

        // Alternative: Set up dom with html template
        // document.body.innerHTML = window.__html__['src/test/form.html'];

        registerButtonHandler();
    });

    afterEach(function () {
        //console.log($(document.body).html());
    });

    it('should extend list when adding item ', function () {

        // Using jQuery since IE8 only supports 'innerText' and FF only supports 'textContent'
        var input = $('#todo-text');
        input.val('First ToDo');

        addToDo();

        var todoListItems = $('#todo-list').children();
        expect(todoListItems.length).toBe(1);

        expect(todoListItems.first().text()).toBe('First ToDo');
    });

    // it('should add item when clicking button', function () {
    //     var input = $('#todo-text');
    //     input.val('First ToDo');
    //
    //     var addBtn = $('#add-button');
    //     addBtn.trigger('click'); // in order for this to work the click handler has to be registered through jQuery
    //
    //     var itemCount = $('#todo-list li').length;
    //     expect(itemCount).toBe(1);
    // });
});


