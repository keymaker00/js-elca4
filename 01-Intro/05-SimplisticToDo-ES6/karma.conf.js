const webpackConfig = require('./webpack.config');

module.exports = function (config) {
    config.set({

        basePath: '',

        frameworks: ['mocha'],

        files: [
            'src/js/**/*.test.js'
        ],

        exclude: [],

        preprocessors: {
            'src/js/**/*.test.js': ['webpack', 'sourcemap']
        },

        webpack: webpackConfig,

        webpackMiddleware: {
            noInfo: true,
            stats: { chunks: false }
        },

        reporters: ['progress'],
        // reporters: ['progress', 'growl'],

        port: 9876,

        colors: true,

        logLevel: config.LOG_INFO,

        autoWatch: true,

        browsers: ['PhantomJS'],
        // browsers: ['Chrome'],
        // browsers: ['Chrome', 'Firefox', 'Safari'],

        singleRun: false,

        concurrency: Infinity
    })
}
