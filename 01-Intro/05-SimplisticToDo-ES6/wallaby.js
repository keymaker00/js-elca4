var webpackConfig = require('./webpack.config');
var wallabyWebpack = require('wallaby-webpack');
var wallabyPostprocessor = wallabyWebpack({});

module.exports = function (wallaby) {
    return {

        files: [
            {pattern: 'src/js/**/*.js', load: false},
            {pattern: 'src/js/**/*.test.js', ignore: true}
        ],

        tests: [
            {pattern: 'src/js/**/*.test.js', load: false},
        ],
        compilers: {
            '**/*.js': wallaby.compilers.babel()
        },
        postprocessor: wallabyPostprocessor,

        setup: function () {
            window.__moduleBundler.loadTests();
        }
    };
};
