import 'babel-polyfill';
import $ from 'jquery';
import {expect} from 'chai';
import {registerButtonHandler, addToDo} from './todo.controller';

describe('ToDo List', () => {
    beforeEach(() => {

        // Programmatically set up dom for test
        document.body.innerHTML = ''; // clean test setup
        const app = document.createElement('div');
        const input = document.createElement('input');
        input.setAttribute('id', 'todo-text');
        const button = document.createElement('button');
        button.setAttribute('id', 'add-button');
        const list = document.createElement('ul');
        list.setAttribute('id', 'todo-list');
        app.appendChild(input);
        app.appendChild(button);
        app.appendChild(list);
        document.body.appendChild(app);

        registerButtonHandler();
    });

    it('extends list when adding an item', () => {

        const input = $('#todo-text');
        input.val('First ToDo');

        addToDo();

        const todoListItems = $('#todo-list').children();

        expect(todoListItems.length).to.equal(1);
        expect(todoListItems.first().text()).to.equal('First ToDo');

        // For Jest:
        // expect(todoListItems.length).toBe(1);
        // expect(todoListItems.first().text()).toBe('First ToDo');
    });

    // it('renders done items', () => {
    //
    //     const input = $('#todo-text');
    //     input.val('First ToDo');
    //
    //     addToDo();
    //
    //     const todoItem = $('#todo-list').children().first();
    //     todoItem.click();
    //
    // })
});
