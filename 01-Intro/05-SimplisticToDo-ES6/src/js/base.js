

import 'jquery';

// Disable from submit... we are a SPA!
$('form').on('submit', function (e) {
    e.preventDefault();
});

// Show the app when all JS has run (styles in the bundel are injected)
$('.cloak').show();
