import 'babel-polyfill';
import 'jquery';
import * as controller from './todo.controller';

export const bootstrap = () => {

    controller.registerButtonHandler();

};

