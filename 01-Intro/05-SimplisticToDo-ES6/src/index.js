'use strict';

import './css/base.css';
import './css/index.css';

import './js/base';
import { bootstrap } from './js/app';

bootstrap();