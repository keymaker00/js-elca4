console.log('Hello World');

import * as $ from 'jquery';

enableTab(location.hash);

function enableTab(hash: string) {
    if (hash === '') hash = '#main';
    $('.container section').hide();
    $(hash).show();
}

window.addEventListener('hashchange', e => {
    enableTab(location.hash)
});
