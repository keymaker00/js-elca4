var jsLessons;
(function (jsLessons) {

    jsLessons.pathPrefix = 'js/';

    jsLessons.lessonFiles = [
        "00-Basics.js",
        "01-Variables.js",
        "02-Strings.js",
        "03-Functions.js",
        "04-Classes.js",
        "05-ObjectLiterals.js",
        "06-DefaultAndRestParameters.js",
        "07-Destructuring.js",
        "08-Spread.js"
    ];

    jsLessons.start();


})(jsLessons || (jsLessons = {}));
