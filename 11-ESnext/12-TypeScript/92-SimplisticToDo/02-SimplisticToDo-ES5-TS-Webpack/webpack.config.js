module.exports = {
    devtool: 'source-map',
    entry: "./src/app.ts",
    output: {
        filename: "build/bundle.js"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },
    module: {
        loaders: [
            {test: /\.ts$/, loader: 'ts', exclude: /node_modules/},
        ]
    }
};