Compile & run:

	npm run clean
	npm run compile
	npm run test

Inspect `build/01-decorate.js`

See how the metadata about the parameters of the decorated function is emitted in the code.