namespace MyApp {

    export class MyThing {
        doSomething(){
            console.log('something');
        }
    }


}


var myThing = new MyApp.MyThing();
myThing.doSomething();


// Note: namespaces and modules are typically not used together ...
