Catch the functional error by introducing types:

    function Greeter(greeting: string) {
        this.greeting = greeting;
    }
    
Alternative:

    interface Greeting {
        message: string
    }
    
    function Greeter(greeting: Greeting) {
        this.greeting = greeting;
    }