For the corresponding TypeScript examples see in the TypeScript directory.

Install the dependencies:

    npm i

## Decorator Example:

Run with babel-node:

    npm run decorator
    
    
    
## Async Example

Run with babel-node:
    
    npm run async
    
To run directly in Chrome enable `Experimantal JavaScript` in `about://flags`



## Bind Example

Run with babel-node:
    
    npm run bind