// Run the script with babel-node
// Make sure you have babel configured for async functions (i.e. presets contains stage-0)

function delayAsync(timeout) {
    console.log('I am going to wait ...');
    return new Promise((resolve, reject) => setTimeout(resolve, timeout));
}

async function main() {
    try {
        var result = await delayAsync(1000);
        console.log('... finished first waiting!');
        result = await delayAsync(1000);
        console.log('... finished second waiting!');

        await (() => new Promise((resolve, reject) => setTimeout(resolve, 5000)))();
    	console.log('... finished third waiting!');

    } catch (e) {
        console.log('ERRORR!', e);
    }
}
main();
console.log('I am not blocking!');
